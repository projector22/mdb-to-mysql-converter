# MDB to MySQL converter

Simple tool for converting MDB databases into MySQL.

## Prerequisites

### Windows

- [WAMP](https://www.wampserver.com/en/download-wampserver-64bits/)
- A copy of Microsoft Office with Microsft Access or at the least [Microsoft Access Database Engine](https://www.microsoft.com/en-us/download/details.aspx?id=54920)

### Linux

***Important:** unfortunately Linux support for accessing MDB files are sketchy at best. There appear to be several drivers which are paid for which you could try and in theory a tool called mdbtools may work - but I've not found a working solution.*

*A better solution is simply to run this tool in a Windows Environment.*

- A LAMP stack and I highly recommend installing [phpMyAdmin](https://www.phpmyadmin.net/)
- ODBC database driver.

```bash
sudo apt install unixodbc php-pdo-odbc mdbtools
```

## Setup

In Linux:

```bash
sudo nano /etc/php/7.4/apache2/php.ini
sudo service apache2 restart
```

In Windows:

*You'll need to change php.ini both from the WAMP icon (for HTTP) and php.ini within **C:\wamp64\bin\php\php7.4.YOUR_VERSION\php.ini** (for CLI). After changes, Restart All Services.*

Uncomment `extension=odbc` & `extension=pdo_odbc` - both are needed.

```ini
;extension=oci8_12c  ; Use with Oracle Database 12c Instant Client
extension=odbc
extension=openssl
;extension=pdo_firebird
extension=pdo_mysql
;extension=pdo_oci
extension=pdo_odbc
;extension=pdo_pgsql
```

## Instructions

The tool has been designed to run through a basic web interface or as a CLI commnand. Either way, you will need to add the database you wish to convert into `src/config.php`.

1. Copy the .mdb database into the `db` folder.
1. Add the name of the database into the config (`src/config.php`), replacing words in CAPS with the relevant data for your database. Something like:

    ```php
    return [
        'databases' => [
            'DATABASE_ID' => realpath( './db/MYDATABASE.mdb' ),
        ],
        'DATABASE_ID' => [
            'database_name' => 'NAME_OF_DB_WITHOUT_EXTENSION',
            'uid' => 'UID_OF_DB_IF_NOT_DEFAULT',
            'pwd' => 'PASSWORD_OF_DB_IF_REQUIRED',
        ],
        'default_db' => 'DATABASE_ID'
    ];
    ```

1. Configure your MySQL server within the config (`src/config.php`). Again replacing words in CAPS with the config. Something like:

    ```php
    return [
        'server'   => '127.0.0.1',
        'username' => 'MYSQL_USERNAME',
        'password' => 'MYSQL_PASSWORD',
    ];
    ```

1. To convert using HTTP, go to the index page of this app, select the database you wish to convert and click the `Convert` button.
1. To convert using CLI, navigate to the installation folder and run:

    ```bash
    php convert.php
    ```

For bigger databases - it's not recommended to execute via HTTP, rather to use CLI. For debugging you can run the command with an output to file as follows:

```bash
php convert.php >> debug.log
```

If you find any other words that cause MySQL errors, please email here:

[incoming+projector22-mdb-to-mysql-converter-14415265-issue-@incoming.gitlab.com](mailto:incoming+projector22-mdb-to-mysql-converter-14415265-issue-@incoming.gitlab.com)

## Futher Reading

- [https://stackoverflow.com/questions/13103712/php-connecting-to-access-database-mdb](https://stackoverflow.com/questions/13103712/php-connecting-to-access-database-mdb)
- [https://www.sitepoint.com/using-an-access-database-with-php/](https://www.sitepoint.com/using-an-access-database-with-php/)
- [https://blog.devart.com/installing-and-configuring-odbc-driver-on-linux.html](https://blog.devart.com/installing-and-configuring-odbc-driver-on-linux.html)
- [https://www.infocaptor.com/microsoft-access-database-using-php-pdo-odbc-example](https://www.infocaptor.com/microsoft-access-database-using-php-pdo-odbc-example)
- [https://stackoverflow.com/questions/34217558/mdbtools-driver-not-returning-string-values-with-php-ms-access](https://stackoverflow.com/questions/34217558/mdbtools-driver-not-returning-string-values-with-php-ms-access)
