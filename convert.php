#!/usr/bin/env php
<?php

/**
 * Convert an MDB file into MySQL.
 * 
 * @author  Gareth Palmer
 * @version 1.0.0
 */

if ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
    die( 'Permission denied. You may not run this file in the browser' );
}

require 'src/functions.php';
$config = require 'src/config.php';

check_odbc();
spl_autoload_register( 'load_class' );

$converter = new Converter( $config );
$converter->convert();
