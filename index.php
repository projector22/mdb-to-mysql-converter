<?php

/**
 * Convert an MDB file into MySQL.
 * 
 * @author  Gareth Palmer
 * @version 1.0.0
 */

require 'src/functions.php';
$config = require 'src/config.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset='UTF-8' />
    <meta name='robots' content='noindex, nofollow'>
    <meta name='googlebot' content='noindex, nofollow'>
    <meta http-equiv='X-Clacks-Overhead' content='GNU Terry Pratchett'>
    <title>MBD to MySQL Converter</title>
    <link rel="stylesheet" type="text/css" href="src/styles/styles.css">
    <link rel="icon" type="image/png" href="favicon.png">
</head>

<body>
    <main>

<?php

check_odbc();
spl_autoload_register( 'load_class' );

echo heading( 1, "MDB to MySQL Converter" );

switch ( isset( $_GET['action'] ) ? $_GET['action'] : null ) {
    case 'convert':
        $converter = new Converter( $config );
        $converter->convert();
        break;
    default:
        echo "<form method='get'>";
        echo "<input type='hidden' name='action' value='convert'>";
        echo "<select name='database'>";
        foreach ( $config['databases'] as $table => $path ) {
            echo "<option>{$table}</table>";
        }
        echo "</select> ";
        echo "<input type='submit' value='Convert'>";
        echo "</form>";
}

?>
    </main>
</body>
</html>