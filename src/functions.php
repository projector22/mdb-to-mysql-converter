<?php

use Db\Controls;

/**
 * Draw an ordered var_dump to the screen
 */
function dump( $data ) {
    echo "<pre>";
    var_dump( $data );
    echo "</pre>";
}

/**
 * Check the database driver is available
 */

function check_odbc() {
    if ( !in_array( 'odbc', PDO::getAvailableDrivers() ) ) {
        throw new Exception( 'ODBC database driver is not available' );
    }
}

/**
 * Autoload classes
 */

function load_class( $class ) {
    $class = str_replace( '\\', '/', $class );
    $load_path = realpath( __DIR__ .  '/classes' ) . '/';
    $path = $load_path . $class . '.php';
    if ( !file_exists ( $path ) ) {
        echo "Error: Class path " . i( $path ) . " does not exist";
        return false;
    }
    require_once realpath( $path );
}

/**
 * Home button
 */

function go_home() {
    if ( !CLI_INTERFACE ) {
        lines( 1 );
        echo "<button onClick='window.location.href = \"index.php\"'>Home</button>";
    }
}


/**
 * Sanitize a table name
 * 
 * @param   string  $entry                      The table name
 * @param   boolean $display_text_feedback      Whether or not to draw text response to the screen.
 * 
 * @return  string
 */

function sanitize_sql_name( $entry, $display_text_feedback = false ) {
    $changed = false;
    foreach ( Controls::ILLEGAL_CHARACTERS as $char ) {
        if ( strpos( $entry, $char ) !== false ) {
            $entry = str_replace( $char, '', $entry );
            $changed = true;
        }
    }
    if ( $changed ) {
        if ( $display_text_feedback ) {
            echo i( b( $entry ) . " contains an illegal character for MySQL. Table renamed to " . b( $entry . '__to_rename' ) );
            lines( 2 );
        }
        return $entry . '__to_rename';
    }

    $numbers = ['0','1','2','3','4','5','6','7','8','9'];

    if ( in_array( $entry[0], $numbers ) ) {
        if ( $display_text_feedback ) {
            echo i( b( $entry ) . " begins with a number, Table renamed to " . b( 'numeric__' . $entry ) );
            lines( 2 ); 
        }
        return 'numeric__' . $entry;
    }
    
    if ( in_array( strtoupper( $entry ), Controls::RESERVED_WORDS, true ) ) {
        if ( $display_text_feedback ) {
            echo i( b( $entry ) . " contains a reserved word for MySQL. Table renamed to " . b( $entry . '__to_rename' ) );
            lines( 2 );
        }
        return $entry . '__to_rename';
    }

    return $entry;
}


/**
 * Draw line breaks according to the condition of the app
 * 
 * @param   int     $k      Number of lines
 */

function lines( $k ) {
    $lb = CLI_INTERFACE ? "\n" : "<br>";
    $line = '';
    for ( $i = 0; $i < $k; $i++ ) {
        $line .= $lb;
    }
    echo $line;
}


/**
 * Set text bold if in HTTP
 * 
 * @param   string  $text
 * 
 * @return  string
 */

function b( $text ) {
    if ( !CLI_INTERFACE ) {
        return "<b>{$text}</b>";
    }
    return $text;
}


/**
 * Set text italics if in HTTP
 * 
 * @param   string  $text
 * 
 * @return  string
 */

function i( $text ) {
    if ( !CLI_INTERFACE ) {
        return "<i>{$text}</i>";
    }
    return $text;
}


/**
 * Set of <h></h> heading tags if in HTTP
 * 
 * @param   string  $text
 * 
 * @return  string
 */

function heading ( $size = 1, $text ) {
    if ( !CLI_INTERFACE ) {
        return "<h{$size}>{$text}</h{$size}>";
    }
    return $text;
}