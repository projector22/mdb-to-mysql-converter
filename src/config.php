<?php

define ( 'CLI_INTERFACE', php_sapi_name() == 'cli' ? true : false );

return [
    'mysql' => [
        'server'   => '127.0.0.1',
        'username' => 'MYSQL_USERNAME',
        'password' => 'MYSQL_PASSWORD',
    ],
    'databases' => [
        'SampleDB'    => realpath( './db/sample-db.mdb' ),
    ],
    'default_db'  => 'SampleDB',
    'SampleDB'      => [
        'database_name' => 'sample-db',
    ],
];