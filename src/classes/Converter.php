<?php

use Db\Controls;
use Db\Connect_ODBC;
use Db\Connect_MySQL;

/**
 * Perform the conversion from MDB to MySQL
 * 
 * use Converter;
 */

class Converter {


    /**
     * The config file
     * 
     * @access  public
     */

    public $config;

    /**
     * The database to convert
     */

    private $db;


    /**
     * Class constructor
     * 
     * @param   array   $config
     * 
     * @access  public
     */

    public function __construct( $config ) {
        $this->config = $config;
        if ( isset( $_GET['database'] ) ) {
            $this->db = $_GET['database'];
        } else {
            $this->db = $this->config['default_db'];
        }
    }


    /**
     * Perform the conversion from MDB to MySQL
     * 
     * @access  public
     */

    public function convert() {
        $start_time = microtime( true );

        ini_set( 'memory_limit', '2048M' );

        $database = $this->config['databases'][$this->db];
        $params   = isset ( $this->config[$this->db] ) ? $this->config[$this->db] : [];
        $odbc = new Connect_ODBC( $database, $params );
        $odbc->get_database_data();

        $database = $this->config[$this->db]['database_name'];
        $params   = $this->config['mysql'];
        $mysql = new Connect_MySQL( $database, $params );

        foreach ( $odbc->tables as $table ) {
            $i = 0;
            echo heading( 2, $table );
            if ( CLI_INTERFACE ) {
                lines( 2 );
            }
            $mysql->create_tables( $table, $odbc->table_schema[$table] );
            
            $headings = implode( ',', $odbc->table_columns[$table] );
            $entries = $odbc->select( "SELECT * FROM `{$table}`" );
            $value = [];
            $k = 0;
            $table = sanitize_sql_name( $table );
            foreach ( $entries as $entry ) {
                foreach ( $entry as $ir => $kr ) {
                    $entry[$ir] = trim( stripslashes( htmlspecialchars( $kr, ENT_QUOTES ) ) );
                }
                $value[] = "('" . implode( "','", $entry ) . "')";
                $i++;
                $k++;
                /**
                 * Work with 15 lines per go
                 */
                if ( $k == 15 ) {
                    $values = implode ( ',', $value );
                    $mysql->insert( "INSERT INTO {$table} ({$headings}) VALUES {$values}" );
                    $k = 0;
                    $value = [];
                }
            }
            if ( count( $value ) > 0 ) {
                $values = implode ( ',', $value );
                $mysql->insert( "INSERT INTO {$table} ({$headings}) VALUES {$values}" );
            }
            lines( 1 );
            echo "{$i} rows entered into {$table}";
            lines( 2 );
        }

        $end_time = microtime( true );
        echo "The conversion took " . ($end_time - $start_time) . " seconds to complete.";

        go_home();
    }
}