<?php

namespace Db;

use Db\Controls;
use \PDO;
use \Exception;
use \PDOException;

/**
 * Handle connections for connecting to a .mdb access database
 * 
 * use Db\Connect_ODBC;
 * 
 * @property    object      $proc_conn
 * @property    string      $database
 * @property    array       $params
 * @property    array       $tables
 * @property    array       $table_schema
 */

class Connect_ODBC extends Controls {

    /**
     * Connection variable to using odbc tools.
     * 
     * @var object  $proc_conn
     * 
     * @access  private
     */

    private $proc_conn;

    /**
     * The full path to the database. Sent from object instantiation.
     * 
     * @var string  $database
     * 
     * @access  private
     */

    private $database;

    /**
     * Any parsed parameters for connecting to the database. Sent from object instantiation.
     * 
     * @var array   $params
     * 
     * @access  private
     */

    private $params;

    /**
     * List of tables in the database.
     * 
     * @var array   $tables
     * 
     * @access  public
     */

    public $tables = [];

    /**
     * List of column heading
     * 
     * @var array   $table_columns
     * 
     * @access  public
     */

    public $table_columns = [];

    /**
     * List of columns in each table.
     * 
     * @var array   $table_schema
     * 
     * @access  public
     */

    public $table_schema = [];

    /**
     * Database password from config
     * 
     * @var string  $password
     * 
     * @access  public
     */

    public $password;


    /**
     * Driver connection string
     * 
     * @access  public
     */

    public $driver_string;


    /**
     * Class constructor
     * 
     * @param   string  $database   Path to the access database to be read;
     * @param   array   $params     Extra parameters to be sent to the connection variable.
     *                              Options:
     *                              - 'uid'
     *                              - 'pwd'
     * 
     * @access  public
     */

    public function __construct( $database, $params = [] ) {
        if ( !is_file( $database ) ) {
            throw new Exception( "{$database} does not exist" );
        }

        $this->database = $database;
        $this->params   = $params;
        
        $uid = isset( $params['uid'] ) ? "Uid={$params['uid']};" : '';
        $pwd = isset( $params['pwd'] ) ? "Pwd={$params['pwd']};" : '';

        $this->password = isset( $params['pwd'] ) ? $params['pwd'] : '';
        
        switch ( PHP_OS ) {
            case 'Linux':
                // $this->driver_string = '';
                echo "Unfortunately there does not appear to be a free open source driver for Linux systems. Unfortuanately the process cannot continue;";
                die;
            case 'WINNT':
                $this->driver_string = 'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};';
                break;                
        }

        try {
            $this->conn = new PDO( "odbc:{$this->driver_string}charset=UTF-8; DBQ={$this->database};{$uid}{$pwd}" );
            $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch( PDOException $e ) {
            echo b( "Error: " ) . $e->getMessage();
            die;
        }
    }
    
    
    /**
     * Set all the database data
     * 
     * @access  public
     */

    public function get_database_data() {
        $this->proc_conn = odbc_connect( "{$this->driver_string}DBQ={$this->database};", 'ADODB.Connection', $this->password );
        $this->get_tables();
        $this->get_table_schema();
    }


    /**
     * Get the table names of the database
     * 
     * @uses odbc_connect rather than PDO.
     * 
     * @access  private
     */

    private function get_tables() {
        $tables = odbc_tables( $this->proc_conn );
		while ( $entry = odbc_fetch_object( $tables ) ) {
            if ( $entry->TABLE_TYPE == 'TABLE' && strpos( $entry->TABLE_NAME, 'MSys' ) === false ) {
			    $this->tables[] = $entry->TABLE_NAME;
		    }
		}
    }


    /**
     * Get the table schema of each table and write to $this->table_schema
     * 
     * @uses odbc_connect rather than PDO.
     * 
     * @access  private
     */

    private function get_table_schema() {
        if ( count ( $this->tables) == 0 ) {
            die( "{$this->database} is empty, halting." );
        }

        foreach ( $this->tables as $table ) {
            $columns = odbc_columns( $this->proc_conn );
            while ( $entry = odbc_fetch_object( $columns ) ) {
                if ( $entry->TABLE_NAME !== $table ) {
                    // Skip schemas which are not directly in the schema
                    continue;
                }
                $entry->COLUMN_NAME = str_replace( ' ', '_', $entry->COLUMN_NAME );
                if ( $entry->TYPE_NAME == 'LONGCHAR' ) {
                    $entry->TYPE_NAME = 'LONGTEXT';
                } else if ( $entry->TYPE_NAME == 'BYTE' ) {
                    $entry->TYPE_NAME = 'VARCHAR';
                } else if ( $entry->TYPE_NAME == 'CURRENCY' ) {
                    $entry->TYPE_NAME = 'VARCHAR';
                } else if ( $entry->TYPE_NAME == 'LONGBINARY' ) {
                    $entry->TYPE_NAME = 'VARBINARY';
                }
                $this->table_schema[$table][$entry->COLUMN_NAME] = $entry;
                $entry->COLUMN_NAME = sanitize_sql_name( $entry->COLUMN_NAME );
                $this->table_columns[$table][] = $entry->COLUMN_NAME;
            }
        }
    }


    /**
     * Class destructor
     * 
     * @access  public
     */

    public function __destruct() {
        if ( isset( $this->conn ) ) {
            $this->conn = null;
        }
    }

}