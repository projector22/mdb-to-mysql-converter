<?php

namespace Db;

use Db\Controls;
use \PDO;
use \PDOException;


/**
 * Handle connections for connecting to a mysql / mariadb database
 * 
 * use Db\Connect_MySQL;
 */

class Connect_MySQL extends Controls {

    /**
     * Database name
     * 
     * @var string  $database
     * 
     * @access  private
     */

    private $database;


    /**
     * Class constructor
     * 
     * @param   string  $db_name    Name of the database to create
     * @param   array   $params     Parameters to be parsed to the class. Must include server, username & password.
     * 
     * @access  public
     */

    public function __construct( $db_name, $params = [] ) {
        $servername = $params['server'];
        $username   = $params['username'];
        $password   = $params['password'];
        $this->database = str_replace( '-', '_', $db_name );

        try {
            $this->conn = new PDO( "mysql:host={$servername};dbname={$this->database}", $username, $password );
            $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        } catch( PDOException $e ) {
            if ( $e->getCode() == 1049 ) {
                try {
                    $conn = new PDO( "mysql:host={$servername}", $username, $password );
                    $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                    $conn->exec( "CREATE DATABASE IF NOT EXISTS {$this->database}" );
                    $conn = null;
                    $this->conn = new PDO( "mysql:host={$servername};dbname={$this->database}", $username, $password );
                    $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                } catch( PDOException $e ) {
                    switch ( $e->getCode() ) {
                        case 1045;
                            echo "Error: One of your database details was incorrect, please try again";
                            break;
                        case 2002:
                            echo "Error: No server can be found at address " . i( $servername ) . " or the server cannot connect";
                            break;
                        default:
                            echo "An error has occured";
                            lines( 1 );
                            echo $e->getMessage();
                    }
                }
            }
        }
    }


    /**
     * Create tables for the database
     * 
     * @param   string  $table      The table name.
     * @param   array   $entries    The table columns.
     * 
     * @access  public
     */

    public function create_tables( $table, $entries ) {
        $table = sanitize_sql_name( $table, true );
        $elements = [];
        $sql = "CREATE TABLE IF NOT EXISTS {$table}(";
        foreach ( $entries as $column ) {
            echo "Creating column " . b( $column->COLUMN_NAME );
            lines( 1 );
            $line = str_replace( '*', '', $column->COLUMN_NAME );
            if ( $column->TYPE_NAME == 'COUNTER' ) {
                $line .= " INT NOT NULL PRIMARY KEY AUTO_INCREMENT";
                $elements[] = $line;
                continue;
            }
            $line .= " {$column->TYPE_NAME}";
            if ( $column->TYPE_NAME == 'VARCHAR' || $column->TYPE_NAME == 'VARBINARY' ) {
                $line .= "($column->COLUMN_SIZE)";
            }
            if ( !$column->NULLABLE ) {
                $line .= " NOT NULL";
            }
            $elements[] = $line;
        }
        $sql .= implode( ',', $elements );
        $sql .= ')';
        $this->execute( $sql );
    }


    /**
     * Class destructor
     * 
     * @access  public
     */

    public function __destruct() {
        if ( isset( $this->conn ) ) {
            $this->conn = null;
        }
    }

}