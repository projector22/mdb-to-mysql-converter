<?php

namespace Db;

use \PDO;
use \PDOException;

/**
 * Class to execute database instructions
 * 
 * use Db\Controls;
 */

class Controls {

    /**
     * The connection object variable
     * 
     * @var object  $conn PDOObject
     * 
     * @access  public
     */

    public $conn;


    /**
     * Perform an sql SELECT statement
     * 
     * @param   string  $sql    A complete SELECT SQL statement
     * 
     * @return  object
     * 
     * @access  public
     */

    public function select( $sql ) {
        $stmt = $this->conn->prepare( $sql );
        $stmt->execute();
        $stmt->setFetchMode( PDO::FETCH_ASSOC );
        return $stmt->fetchAll();
    }


    /**
     * Perform an SQL INSERT
     * 
     * @param   string  $sql    A complete INSERT SQL statement
     * 
     * @return  boolean
     * 
     * @access  public
     */

    public function insert( $sql ) {
        return $this->execute( $sql );
    }


    /**
     * Perform an SQL UPDATE
     * 
     * @param   string  $sql    A complete UPDATE SQL statement
     * 
     * @return  boolean
     * 
     * @access  public
     */

    public function update( $sql ) {
        return $this->execute( $sql );
    }


    /**
     * Perform an SQL DELETE
     * 
     * @param   string  $sql    A complete DELETE SQL statement
     * 
     * @return  boolean
     * 
     * @access  public
     */

    public function delete( $sql ) {
        return $this->execute( $sql );
    }


    /**
     * Execute an SQL statement
     * 
     * @param   string  $sql    A complete SQL statement
     * 
     * @return  boolean
     * 
     * @access  public
     */

    public function execute( $sql ) {
        try {
            $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $this->conn->exec( $sql );
            return true;
        } catch( PDOException $e ) {
            echo $e->getMessage();
            lines( 2 );
            echo b( $sql );
            lines( 2 );
            return false;
        }
    }


    /**
     * MySQL Reserved words
     * 
     * @var array   RESERVED_WORDS
     * 
     * @since   MySQL Version 8.0.27
     */

    const RESERVED_WORDS = [
        'ACTIVE', 'ADMIN', 'ARRAY', 'ATTRIBUTE', 'AUTHENTICATION', 'BUCKETS', 'CHALLENGE_RESPONSE', 'CLONE', 'COMPONENT', 'CUME_DIST', 
        'DEFINITION', 'DENSE_RANK', 'DESCRIPTION', 'EMPTY', 'ENFORCED', 'ENGINE_ATTRIBUTE', 'EXCEPT', 'EXCLUDE', 'FACTOR', 'FAILED_LOGIN_ATTEMPTS',
        'FINISH', 'FIRST_VALUE', 'FOLLOWING', 'GEOMCOLLECTION', 'GET_MASTER_PUBLIC_KEY', 'GET_SOURCE_PUBLIC_KEY', 'GROUPING', 'GROUPS', 'HISTOGRAM', 
        'HISTORY', 'INACTIVE', 'INITIAL', 'INITIATE', 'INVISIBLE', 'JSON_TABLE', 'JSON_VALUE', 'KEYRING', 'LAG', 'LAST_VALUE', 'LATERAL', 'LEAD', 
        'LOCKED', 'MASTER_COMPRESSION_ALGORITHMS', 'MASTER_PUBLIC_KEY_PATH', 'MASTER_TLS_CIPHERSUITES', 'MASTER_ZSTD_COMPRESSION_LEVEL', 'MEMBER', 
        'NESTED', 'NETWORK_NAMESPACE', 'NOWAIT', 'NTH_VALUE', 'NTILE', 'NULLS', 'OF', 'OFF', 'OJ', 'OLD', 'OPTIONAL', 'ORDINALITY', 'ORGANIZATION', 
        'OTHERS', 'OVER', 'PASSWORD_LOCK_TIME', 'PATH', 'PERCENT_RANK', 'PERSIST', 'PERSIST_ONLY', 'PRECEDING', 'PRIVILEGE_CHECKS_USER', 'PROCESS',
        'RANDOM', 'RANK', 'RECURSIVE', 'REFERENCE', 'REGISTRATION', 'REPLICA', 'REPLICAS', 'REQUIRE_ROW_FORMAT', 'RESOURCE', 'RESPECT', 'RESTART', 
        'RETAIN', 'RETURNING', 'REUSE', 'ROLE', 'ROW_NUMBER', 'SECONDARY', 'SECONDARY_ENGINE', 'SECONDARY_ENGINE_ATTRIBUTE', 'SECONDARY_LOAD', 
        'SECONDARY_UNLOAD', 'SKIP', 'SOURCE_AUTO_POSITION', 'SOURCE_BIND', 'SOURCE_COMPRESSION_ALGORITHMS', 'SOURCE_CONNECT_RETRY', 'SOURCE_DELAY', 
        'SOURCE_HEARTBEAT_PERIOD', 'SOURCE_HOST', 'SOURCE_LOG_FILE', 'SOURCE_LOG_POS', 'SOURCE_PASSWORD', 'SOURCE_PORT', 'SOURCE_PUBLIC_KEY_PATH', 
        'SOURCE_RETRY_COUNT', 'SOURCE_SSL', 'SOURCE_SSL_CA', 'SOURCE_SSL_CAPATH', 'SOURCE_SSL_CERT', 'SOURCE_SSL_CIPHER', 'SOURCE_SSL_CRL', 
        'SOURCE_SSL_CRLPATH', 'SOURCE_SSL_KEY', 'SOURCE_SSL_VERIFY_SERVER_CERT', 'SOURCE_TLS_CIPHERSUITES', 'SOURCE_TLS_VERSION', 'SOURCE_USER', 
        'SOURCE_ZSTD_COMPRESSION_LEVEL', 'SRID', 'STREAM', 'SYSTEM', 'THREAD_PRIORITY', 'TIES', 'TLS', 'UNBOUNDED', 'UNREGISTER', 'VCPU', 'VISIBLE',
        'WINDOW', 'ZONE', 'POSITION', 'LEAVE', 'GROUP', 'ORDER', 'TO', 'GET', 'ASC', 'DESC', 'OPTION', 'PRIMARY', 'CONDITION', 'KEY', 'FLOAT',
    ];


    /**
     * MrSQL Illegal characters
     * 
     * @var array   ILLEGAL_CHARACTERS
     * 
     * @since   MySQL Version 8.0.27
     */

    const ILLEGAL_CHARACTERS = [
        '*', '(', ')', ' ', '<', '>', '-'
    ];

}